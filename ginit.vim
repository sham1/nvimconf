" Enable mouse
set mouse=a

" Disable tablines and such
if exists(':GuiTabline')
	GuiTabline 0
endif

if exists(':GuiPopupmenu')
	GuiPopupmenu 0
endif

if exists(':GuiScrollBar')
	GuiScrollBar 0
endif

" Enable ligatures if they exist
if exists(':GuiRenderLigatures')
	GuiRenderLigatures 1
endif

" Use a base16 theme
packadd! base16-vim
colorscheme base16-material-palenight

" Use cursorline
set cursorline
